var path = require('path');
var CopyWebpackPlugin = require('copy-webpack-plugin');

var config = () => {
    return{
        entry: './src/components/main.js',
        output: {
            path:path.resolve(__dirname,'dist'),
            filename: 'bundle.js'
        },
        module:{
            rules:[
                {
                    test:/\.js$/,
                    exclude:/node_modules/,
                    loader:'babel-loader',
                    query:{
                        presets:['es2015','react']
                    }
                },
                {
                    test:/\.(s*)css$/,
                    use:['style-loader','css-loader', 'sass-loader']
                },
                // {
                //     test:/\.(png|svg|jpg|jpeg)$/,
                //     loader:'file-loader',
                //     options:{
                //         name:'images/[name].[ext]'
                //     }
                // }
            ]
        },
        plugins:[ 
            new CopyWebpackPlugin([
                {from:'./public/images',to:'images'} 
            ])
        ], 
    }
}
 
 module.exports = config;