import React,{Component} from 'react';
import Component2 from './component2';

class Component1 extends Component{
    render(){
        return (
            <div>
                <h1>Component1 content </h1>
                <Component2 />
            </div>
        )
    }
}

export default Component1;