import React,{Component} from 'react';
import Component1 from './component1';
import styles from "../styles/styles.scss"

class App extends Component{
    render(){
        return (
            <div id="header-wrapper">
                <h1 id="header">React Application</h1>
                <Component1 />
            </div>
        )
    }
}

export default App;